import {Component, OnInit} from '@angular/core';
import axios from 'axios';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private router: Router) {
  }

  onSubmit() {
    const router = this.router;
    axios.post('http://localhost:6533/login', `login=${this.loginForm.value.login}&password=${this.loginForm.value.password}`, {
      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      withCredentials: true
    }).then(function (response) {
      router.navigate(['/editor']);
    }).catch(function (error) {
      console.log(error);
    });
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      login: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

}
