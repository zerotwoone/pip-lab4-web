import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {PlotComponent} from './plot/plot.component';
import {WelcomeComponent} from './welcome/welcome.component';
import {EditorComponent} from './editor/editor.component';
import {AppRoutingModule} from './/app-routing.module';
import {ClockComponent} from './clock/clock.component';
import {HttpClientModule} from '@angular/common/http';
import { AuthComponent } from './auth/auth.component';

@NgModule({
  declarations: [
    AppComponent,
    PlotComponent,
    WelcomeComponent,
    EditorComponent,
    ClockComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
