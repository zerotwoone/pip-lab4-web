export class HitResult {
  x = '0';
  y = '0';
  r = '1';
  inArea = false;

  constructor(x, y, r, inArea) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.inArea = inArea;
  }
}
